import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.charset.Charset;

public class DeleteAndPost extends BaseClass {

    @Test
    public void deleteIsSuccessful() throws IOException {


        HttpDelete request = new HttpDelete("https://api.github.com/repos/adrejss88/deleteme");

        request.setHeader(HttpHeaders.AUTHORIZATION, "token");
        response = client.execute(request);

        int actualStatusCode = response.getStatusLine().getStatusCode();

        Assert.assertEquals(actualStatusCode,204 );
    }

    @Test
    public void createReporeturns201() throws IOException {

        HttpPost request = new HttpPost("https://api.github.com/users/repos");

        String auth = "email" + "password";
        byte[] encodeAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("ISO-8859-1")));
        String authHeader = "Basic" + new String(encodeAuth);
        String json = "(\"name\"; \"deleteme\"";

        request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));

        response = client.execute(request);

        int actualStatusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(actualStatusCode, 201);

    }
}
